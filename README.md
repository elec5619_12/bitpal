# README for BITPAL #

git clone https://<username>@bitbucket.org/elec5619_12/bitpal.git
load into Spring and runserver.
Ensure all dependencies are downloaded, if not. Right click on project -> Maven -> Update Project -> Okay.

[View the BitPal wiki here](https://bitbucket.org/elec5619_12/bitpal/wiki/Home)


### What is this repository for? ###

* The repository is for ELEC5619 Semester 2 Project on Positive Computing named BitPal
* Version 1 (Still in development)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
