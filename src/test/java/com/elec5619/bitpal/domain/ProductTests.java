package com.elec5619.bitpal.domain;

import junit.framework.TestCase;

import com.elec5619.bitpal.domain.Type;


public class ProductTests extends TestCase{
	
	private Product product;
	
	public void setUp() throws Exception {
		product = new Product();
	}


	public void testSetAndGetParameters() {
		
		product.setName("Hello");
		product.setPrice(100);
		product.setType(Type.BOOTS);

		assertEquals("Hello",product.getName());
		assertEquals(100.0,product.getPrice());
		assertEquals(Type.BOOTS,product.getType());

	}
	
	public void testNullParameters() {
		
		assertNull(null,product.getName());
		assertNull(null,product.getPrice());
		assertNull(null,product.getType());
		

	}
	
}
