package com.elec5619.bitpal.web;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.ui.Model;

import com.elec5619.bitpal.domain.User;

public class BattleControllerTest {

	@Test
	public void test() {
		User challenger1 = new User();
		User challenger2 = new User();
		challenger1.setExperience(1);
		challenger2.setExperience(2);
		challenger1.setHealth(3);
		challenger2.setHealth(4);
		challenger1.setAttack(5);
		challenger2.setAttack(6);
		challenger1.setDefense(7);
		challenger2.setDefense(8);
		assertEquals(challenger1.getExperience(), 1);
		assertEquals(challenger2.getExperience(), 2);
		assertEquals(challenger1.getHealth(), 3);
		assertEquals(challenger2.getHealth(), 4);
		assertEquals(challenger1.getAttack(), 5);
		assertEquals(challenger2.getAttack(), 6);
		assertEquals(challenger1.getDefense(), 7);
		assertEquals(challenger2.getDefense(), 8);
		
	
		
	}

}
