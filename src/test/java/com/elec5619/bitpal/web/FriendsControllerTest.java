package com.elec5619.bitpal.web;

import junit.framework.TestCase;

import java.util.List;

import javax.annotation.Resource;


import com.elec5619.bitpal.domain.User;
import com.elec5619.bitpal.service.DatabaseTestUserManager;
import com.elec5619.bitpal.service.DatabaseUserManager;
import com.elec5619.bitpal.service.UserManager;

public class FriendsControllerTest extends TestCase {
	DatabaseTestUserManager databaseUserManager;
	
	public void setUp() throws Exception {
		databaseUserManager = new DatabaseTestUserManager();
		User user =new User();
		//
		databaseUserManager.addUser(user);
	}

	
	public void testFriends(){
		User friend = new User();
		User self = databaseUserManager.getUserById(1);

		//Test boolean friend 
		friend.setFriend(true);
		assertEquals(friend.isFriend(), true);		

		// Test friend exists in list of friend
		databaseUserManager.addFriend(friend);
		List<User> friendsList = databaseUserManager.getFriends();
		assertTrue(friendsList.contains(friend));
	}

}
