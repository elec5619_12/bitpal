package com.elec5619.bitpal.service;

import org.joda.time.LocalDate;
import org.json.JSONException;
import org.json.JSONObject;

import com.fitbit.api.common.model.user.Gender;
import com.fitbit.api.common.model.user.UserInfo;

public class UserInfoTest extends UserInfo {
	UserInfo ui = new UserInfo(null);
	

	public UserInfoTest(JSONObject json) throws JSONException {
		super(json);
		// TODO Auto-generated constructor stub
	}
	
	public String getDistanceUnit() {
		return "m";
	}
	public String getHeightUnit() {
		return "cm";
	}
	public String getWeightUnit() {
		return "kg";
	}
	public String getWaterUnit() {
		return "mL";
	}
	
	public String getLocale() {
		return "aus";
	}
	
	public String getTimezone() {
		return "";
	}

	
	public Gender getGender() {
		return Gender.MALE;
	}
	
	public double getHeight() {
		return 190.0;
	}
	
	public double getWeight() {
		return 90.0;
	}
	
	public String getEncodedId() {
		return "12345";
	}
	
	public String getDisplayName() {
		return "displayName";
	}
	
	public String getDateOfBirth() {
		return new LocalDate(2014, 11, 11).toString();
	}

}
