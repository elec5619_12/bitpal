package com.elec5619.bitpal.service;

import static org.junit.Assert.*;

import org.junit.Test;

import com.elec5619.bitpal.domain.User;

public class DatabaseUserManagerTest {

	@Test
	public void testAttack() {
		User user = new User();
		user.setAttack(4);
		assertEquals(user.getAttack(), 4);
	}
	
	@Test
	public void testBalance() {
		User user = new User();
		user.setBalance(10);
	}
	
	@Test
	public void testDOB() {
		User user = new User();
		user.setDateOfBirth("14th August");
		assertEquals(user.getDateOfBirth(), "14th August");
	}
	
	@Test
	public void testDefense() {
		User user = new User();
		user.setDefense(10);
		assertEquals(user.getDefense(), 10);
	}
	
	@Test
	public void testDistanceUnit() {
		User user = new User();
		user.setDistanceUnit("10");
		assertEquals(user.getDistanceUnit(), "10");
	}
	
	@Test
	public void testExperience() {
		User user = new User();
		user.setExperience(10);
		assertEquals(user.getExperience(), 10);
	}
	
	@Test
	public void testFriend() {
		User user = new User();
		user.setFriend(true);
		assertTrue(user.isFriend());
	}
	
	@Test
	public void testHealth() {
		User user = new User();
		user.setHealth(10);
		assertEquals(user.getHealth(), 10);
	}
	
	@Test
	public void testHeight() {
		User user = new User();
		user.setHeight(10.10);
		assertEquals(user.getHeight(), 10.10, 0);		
	}
	
	@Test
	public void testHeightUnit() {
		User user = new User();
		user.setHeightUnit("cm");
		assertEquals(user.getHeightUnit(), "cm");		
	}
	
	@Test
	public void testId() {
		User user = new User();
		user.setId(123456789);
		assertEquals(user.getId(), 123456789);	
	}
	
	@Test
	public void testLastUpdated() {
		User user = new User();
		user.setLastUpdated("10:52");
		assertEquals(user.getLastUpdated(), "10:52");	
	}
	
	@Test
	public void testLocale() {
		User user = new User();
		user.setLocale("locale");
		assertEquals(user.getLocale(), "locale");	
	}
	
	@Test
	public void testMemberSince() {
		User user = new User();
		user.setMemberSince("24-11-13");
		assertEquals(user.getMemberSince(), "24-11-13");	
	}
	
	@Test
	public void testPassword() {
		User user = new User();
		user.setPassword("password");
		assertEquals(user.getPassword(), "password");	
	}
	

}
