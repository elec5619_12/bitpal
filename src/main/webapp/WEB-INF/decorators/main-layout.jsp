<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>BitPal</title>
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="ELEC5619 2014sem2 Group Project">
  <meta name="author" content="Group12">
  <link href="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"
        rel="stylesheet"  type="text/css" />
  <link href="<c:url value="/resources/css/main.css" />"
        rel="stylesheet"  type="text/css" />
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</head>

<body>
    <div id="wrap">

		<!-- import NAVBAR -->
        <c:import url="/WEB-INF/views/tags/navbar.jsp"/>

        <div class="container">
            <div class="row">
                    <!-- import HEADER -->
                    <c:import url="/WEB-INF/views/tags/header.jsp"/>

                    <div class="row">
                      <div class="col-md-12">

                        <hr class="soften">

						<!-- import BODY (anything returned by a controller) -->
                        <decorator:body />

                      </div>
                    </div><!--/col-->
                </div><!--/row-->
          </div><!--/row-->

          <hr class="soften">
    </div>

    <c:import url="/WEB-INF/views/tags/footer.jsp"/>
	
    <!-- <script type="text/javascript" src="<c:url value="/resources/js/demo.js" />"></script>  -->
	<!-- <script type="text/javascript" src="<c:url value="/resources/js/json2.js" />"></script>  -->
    <!-- <script type="text/javascript" src="<c:url value="/resources/js/date.format.js" />"></script>  -->
</body>
</html>
