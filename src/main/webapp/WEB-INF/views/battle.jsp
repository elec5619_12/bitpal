<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Battle</title>
</head>
<body>
	<h1 align="center"><c:out value="${self.username} vs. ${friend.username}"/> </h1>
	<c:if test="${friendScore > userScore}">
		<div align="center"> <img src="<c:url value= "/resources/Images/lost.gif"/>" id="fight"/></div>
	</c:if>
	
	<c:if test="${friendScore < userScore}">
	<div align="center"> <img src="<c:url value= "/resources/Images/won.gif"/>" id="fight"/></div>
	</c:if>
	
	<c:if test="${friendScore == userScore}">
		<div align="center"> <img src="<c:url value= "/resources/Images/draw.gif"/>" id="fight"/></div>
	</c:if>


	</body>
</html>
