<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
</head>
<body>
<h1>
	FitBit Data from your device
</h1>
<c:out value="${request}"/>
<P>   </P>

<p></p>
<h3>Data</h3>
		<c:choose>
			<c:when test="${user == Null}">
				<p> No Users Data</p>
			</c:when>
			<c:otherwise>
	        	<c:out value="${userTags[0]}"/>: <c:out value="${user.aboutMe}"/><br></br>
	        	<c:out value="${userTags[1]}"/>: <c:out value="${user.avatar}"/><br></br>
	        	<c:out value="${userTags[2]}"/>: <c:out value="${user.city}"/><br></br>
	        	<c:out value="${userTags[3]}"/>: <c:out value="${user.country}"/><br></br>
	        	<c:out value="${userTags[4]}"/>: <c:out value="${user.dateOfBirth}"/><br></br>
	        	<c:out value="${userTags[5]}"/>: <c:out value="${user.displayName}"/><br></br>
	        	<c:out value="${userTags[6]}"/>: <c:out value="${user.distanceUnit}"/><br></br>
	        	<c:out value="${userTags[7]}"/>: <c:out value="${user.fullName}"/><br></br>
	        	<c:out value="${userTags[8]}"/>: <c:out value="${user.gender}"/><br></br>
	        	<c:out value="${userTags[9]}"/>: <c:out value="${user.height}"/><br></br>
	        	<c:out value="${userTags[10]}"/>: <c:out value="${user.heightUnit}"/><br></br>
	        	<c:out value="${userTags[11]}"/>: <c:out value="${user.locale}"/><br></br>
	        	<c:out value="${userTags[12]}"/>: <c:out value="${user.memberSince}"/><br></br>
	        	<c:out value="${userTags[13]}"/>: <c:out value="${user.nickname}"/><br></br>
	        	<c:out value="${userTags[14]}"/>: <c:out value="${user.state}"/><br></br>
	        	<c:out value="${userTags[15]}"/>: <c:out value="${user.timezone}"/><br></br>
	        	<c:out value="${userTags[16]}"/>: <c:out value="${user.waterUnit}"/><br></br>
	        	<c:out value="${userTags[17]}"/>: <c:out value="${user.weight}"/><br></br>
	        	<c:out value="${userTags[18]}"/>: <c:out value="${user.weightUnit}"/><br></br>
	        	

	        	 	Steps: <c:out value="${userActivities.summary.steps}" /><br/>
	        	<br></br>
        	</c:otherwise>
        </c:choose>
 <p></p>
</body>
</html>
