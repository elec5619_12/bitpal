<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Avatar Customization</title>
</head>
<body>
    <div class="jumbotron">
        <h1 class="text-center">Avatar Customization</h1>
        <p class="text-center">Here you can customize the items on you Avatar!</p>
   	</div>

<!-- <form name="populate" action="avatarcustomization.htm" method="post"> -->
<!-- 	<input type="submit" value="Populate database"> -->
<!-- </form> -->
<h3><button class="btn btn-lg btn-success btn-block">Equipped Items</button></h3>
	<c:if test="${items.size() == 0}">
		<p class="text-center bg-danger">No Items found in database</p>
	</c:if>
<table class="table table-hover table-condensed bg-success">
  <thead>
  <tr>
  	<th>Name</th>
    <th>Price</th>
    <th>Type</th>		
    <th>Unequip?</th>
  </tr>
  </thead>
  <tbody>
	<c:forEach items="${items}" var="item">
		<tr>
		<c:if test="${item.equipped == true }">
		<td><c:out value="${item.name}"/></td>
		<td><c:out value="${item.price}"/></td>
		<td><c:out value="${item.type}"/></td>
		<td>
			<form name="toggle" action="avatarcustomization.htm" method="post">
				<input type="hidden" Value="${item.id}" name="id">
				<input type="submit" Value="Unequip" name="current">
			</form>
		</td>
		</c:if>
		</tr>
	</c:forEach>
	</tbody>
</table>
<br />
<br />
<h3><button class="btn btn-lg btn-info btn-block">Inventory</button></h3>
<table class="table table-hover table-condensed bg-info">
<thead>
  <tr>
  	<th>Name</th>
    <th>Price</th>
    <th>Type</th>		
    <th>Equip?</th>
  </tr>
</thead>
<tbody>
	<c:forEach items="${items}" var="item">
		<tr>
		<c:if test="${item.equipped == false }">
		<td><c:out value="${item.name}"/></td>
		<td><c:out value="${item.price}"/></td>
		<td><c:out value="${item.type}"/></td>
		<td>
			<form name="toggle" action="avatarcustomization.htm" method="post">
				<input type="hidden" Value="${item.id}" name="id">
				<input type="submit" Value="Equip" name="current">
			</form>
		</td>
		</c:if>
		</tr>
	</c:forEach>
</tbody>
</table>
</body>
</html>