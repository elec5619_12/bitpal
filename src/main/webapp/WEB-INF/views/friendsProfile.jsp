<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<!--<script type=text/javascript src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular.min.js"></script>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script> -->
<script type="text/javascript">
$(document).ready(function () {
var $rows = $('#friends tr');
$('#search').keyup(function() {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
    
    $rows.show().filter(function() {
        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();
})
});
</script>
<title>YourWorstNightmare</title>

</head>
<body>
	<h1><c:out value="Viewing ${friend.username}'s Profile"/></h1>
	<h2> Skills</h2>
	<table class="table table-hover table-condensed bg-success">
    	<tr align="center">
    		<th width = 20%>Trait</th>
    		<th width = 20%>Score</th>  
	  	</tr>
	  	<tr align="center">
    		<td width = 20%>Experience</td>
    		<td width = 20%><c:out value="${friend.experience}"/></td>  
	  	</tr>   
	  	<tr align="center">
    		<td width = 20%>Health</td>
    		<td width = 20%><c:out value="${friend.health}"/></td>  
	  	</tr>
	  	<tr align="center">
    		<td width = 20%>Defensive Skill</th>
    		<td width = 20%><c:out value="${friend.defense}"/></th>  
	  	</tr> 
	  	<tr align="center">
    		<td width = 20%>Attacking Skill</th>
    		<td width = 20%><c:out value="${friend.attack}"/></th>  
	  	</tr>  
	</table>
	<h2> Battle Stats</h2>
	<table class="table table-hover table-condensed bg-success">
    	<tr align="center">
    		<th width = 20%>Statistic</th>
    		<th width = 20%>Score</th>  
	  	</tr>
	  	<tr align="center">
    		<td width = 20%>Wins</th>
    		<td width = 20%><c:out value="7"/></th>  
	  	</tr>   
	  	<tr align="center">
    		<td width = 20%>Losses</th>
    		<td width = 20%><c:out value="5"/></th>  
	  	</tr>
	  	<tr align="center">
    		<td width = 20%>Win/Loss Ratio</th>
    		<td width = 20%><c:out value="58.3%"/></th>  
	  	</tr>  
	</table>
	<div align="center">
		<form name="battle" action="battle" method="post">
						<input type="hidden" Value="${friend.username}" name="friend">
						<input type="hidden" Value="${self.username}" name="self">
						<input type="submit" Value="Battle ${friend.username}!" name="current" class="myButton">
					</form>
</body>
</html>