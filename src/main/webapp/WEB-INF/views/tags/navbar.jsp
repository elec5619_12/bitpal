<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">                                   
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">BitPal</a>
        </div>
        <div class="navbar-collapse collapse">  
          <ul class="nav navbar-nav">
            <li class="active"><a href="<c:url value="/" />">Home</a></li>
            <li><a href="<c:url value="/register" />">Register</a></li>
            <li><a href="<c:url value="/login" />">Login</a></li>
            <li><a href="<c:url value="/shop" />">Store</a></li>
            <li><a href="<c:url value="/friends" />">Friends</a></li>
            <li><a href="<c:url value="/avatarcustomization" />">Avatar Customization</a></li>
                 <li><a href="<c:url value="/about" />">About</a></li>
            
          </ul>
          <c:if test="${AuthenticatedUser != null}">
		  <form action="logout" method="post">
          	<button style="padding-bottom: 15px; padding-top: 15px; float: right;" type="submit" class="btn btn-default">Log out</button>
          </form>
          </c:if>
        </div>   			      		 
  </div>
</div>