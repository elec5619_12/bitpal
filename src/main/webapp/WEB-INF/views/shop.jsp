<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<script>
function msg()
{
alert("You need to login to make any purchases");

}
</script>


<div class="container-fluid">
    <div class="jumbotron">
        <h1>Avatar Store</h1>
        <p>Check out the current items we have in store!</p>
     </div>
<p></p>

<h2 align="center">  Balance: $${balance} </h2>
<h3><button class="btn btn-lg btn-primary btn-block">Categories</button></h3> <br>

		
		
	<table class="table table-hover table-condensed">
   <caption><button type="button" class="btn btn-default  btn-success btn-block"> Tops</button> <br></caption>
   <thead>
   <tr>
  	<th>Name</th>
    <th>Price</th>		
  </tr>
   </thead>
   <tbody>
   		<c:forEach items="${tops}" var="top">
      	<tr>  	
      	<td> <c:out value="${top.name}" /> </td>
        <td> $<c:out value="${top.price}" /> </td>
        <td align="right"> 
        	<form id="name" action="shop.htm" method="POST">
				<input type="hidden" Value="${top.id}" name="name">
				<input class="btn btn-default" type="submit" Value="Purchase" name="name">
			</form>
       	</td>
        
      	</tr>
      	</c:forEach>
   </tbody>
</table>
<!--   -->

		
<table class="table table-hover table-condensed">
   <caption><button type="button" class="btn btn-default  btn-block btn-danger"> Bottoms</button> <br></caption>
   <thead>
     <tr>
  	<th>Name</th>
    <th>Price</th>		
  </tr>
   </thead>
   <tbody>
   		<c:forEach items="${pants}" var="bottoms">
      	<tr>  	
      	<td> <c:out value="${bottoms.name}" /> </td>
        <td> $<c:out value="${bottoms.price}" /> </td>
        <td align="right"> 
        	<form id="name" action="shop.htm" method="POST">
				<input type="hidden" Value="${bottoms.id}" name="name">
				<input class="btn btn-default" type="submit" Value="Purchase" name="name">
			</form>
       	</td>
      	</tr>
      	</c:forEach>
   </tbody>
</table>

<table class="table table-hover table-condensed">
	<caption><button type="button" class="btn btn-default  btn-block btn-info" > Weapons</button> </caption>
	<thead>
	<tr>
  	<th>Name</th>
    <th>Price</th>		
  </tr>
  </thead>
   <tbody>
   		<c:forEach items="${weapons}" var="weapon">
      	<tr>  	
      	<td> <c:out value="${weapon.name}" /> </td>
        <td> $<c:out value="${weapon.price}" /> </td>
        <td align="right"> 
        	<form id="name" action="shop.htm" method="POST">
				<input type="hidden" Value="${weapon.id}" name="name">
				<input class="btn btn-default" type="submit" Value="Purchase" name="name">
			</form>
       	</td>
      	</c:forEach>
   </tbody>
</table>
	
<table class="table table-hover table-condensed">
	<caption><button type="button" class="btn btn-default  btn-block btn-warning" > Shoes</button> </caption>
	<thead>     <tr>
  	<th>Name</th>
    <th>Price</th>		
  </tr>
  </thead>
   <tbody >
   		<c:forEach items="${boots}" var="boot">
      	<tr>  	
      	<td> <c:out value="${boot.name}" /> </td>
        <td> $<c:out value="${boot.price}" /> </td>
        <td align="right"> 
        	<form id="name" action="shop.htm" method="POST">
				<input type="hidden" Value="${boot.id}" name="name">
				<input class="btn btn-default" type="submit" Value="Purchase" name="name">
			</form>
       	</td>
      	</c:forEach>
   </tbody>
</table>	

<table class="table table-hover table-condensed">
	<caption><button type="button" class="btn btn-default  btn-block btn-primary" > Hats</button> </caption>
	<thead>
	<tr>
  	<th>Name</th>
    <th>Price</th>		
  </tr>
	</thead>
   <tbody>
   		<c:forEach items="${hat}" var="hat">
      	<tr>  	
      	<td> <c:out value="${hat.name}" /> </td>
        <td> $<c:out value="${hat.price}" /> </td>
        <td align="right"> 
        	<form id="name" action="shop.htm" method="POST">
				<input type="hidden" Value="${hat.id}" name="name">
				<input class="btn btn-default" type="submit" Value="Purchase" name="name">
			</form>
       	</td>
      	</c:forEach>
   </tbody>
</table>	

</div>
 <br>
 <br>



 






 
 <div class="alert alert-info alert-sm" style="text-align:center"><a href="<c:url value="/avatarcustomization" />">View Inventory</a></div>
</body>
</html>
