<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<!-- <script type=text/javascript src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular.min.js"></script>-->
<!-- <script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>-->
<script type="text/javascript">
/*$(document).ready(function () {
	var $rows = $('#users tr');
	$('#searchUsers').keyup(function() {
	    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

	    $rows.show().filter(function() {
	        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
	        return !~text.indexOf(val);
	    }).hide();
	});
}); */
</script>
<title>Friends</title>

</head>
<body>
  <div class="jumbotron">
        <h1 class="text-center">Friends</h1>
        <p class="text-center">Add or remove friends and view their profiles!</p>
   	</div>
   	<h3>Friends</h3>
	<!--  Table search of friends -->
	<c:if test="${friends.size() == 0}">	
		<p> You have no friends. Add some from below! </p>
	</c:if>

	<c:if test="${friends.size() != 0}">
		<!--<div style='text-align: center'><input type="text" id="search" placeholder="Type to search" style="margin: 0px auto; width: 90%; font-size: 14pt;"></div>-->
		<table class="table table-hover table-condensed bg-success">
     	<tr align="center">
    		<th width = 20%>Username</th>
    		<th width = 20%>Experience</th>  
	    	<th width = 20%>Delete?</th>
	    	<th width = 20%>View Profile?</th>    
	  	</tr>  	
  	 
    	 <c:forEach items="${friends}" var="friend">
	     	<tr>
		     	<td><c:out value="${friend.username}"/></td>
				<td><c:out value="${friend.experience}"/></td>     	
		     	<td>
					<form name="addFriend" action="friends.htm" method="post">
						<input type="hidden" Value="${friend.id}" name="id">
						<input type="submit" Value="Delete Friend" name="current">
					</form>
				</td>
				<td>
					<form name="viewFriend" action="friendsProfile" method="post">
						<input type="hidden" Value="${friend.id}" name="id">
						<input type="hidden" Value="${self.username}" name="self">
						<input type="submit" Value="View Profile" name="current">
					</form>
				</td>
	     	</tr>
     	</c:forEach>
     	</table>
     	</c:if>
     
  	
 
     
	
		
	
		<!-- Table of all users -->
	<h3>Add new friends</h3>
		<!--  Table search of friends -->
	<c:if test="${users.size() == 0}">
		<p> No users are registered </p>
	</c:if>
	<c:if test="${users.size() != 0}">
		<!--<div style='text-align: center'><input type="text" id="search" placeholder="Type to search" style="margin: 0px auto; width: 90%; font-size: 14pt;"></div>-->
		<table class="table table-hover table-condensed bg-success">
	     <tr align="center">
	    	<th width = 20%>Username</th>
	    	<th width = 20%>Experience</th>  
	    	<th width = 20%>Add?</th>    
	  	</tr>
	     <c:forEach items="${users}" var="user">
		     <tr>
		     <c:if test="${user.friend == false && user.username != self.username}">
		     	<td><c:out value="${user.username}"/></td>
				<td><c:out value="${user.experience}"/></td>
		     	<td>
					<form name="addFriend" action="friends.htm" method="post">
						<input type="hidden" Value="${user.id}" name="id">
						<input type="submit" Value="Add Friend" name="current">
					</form>
				</td>
			</c:if>
     		</tr>
     	</c:forEach>
     	</table>
     </c:if>
     
	
</body>

</html>



<!-- OLD CODE -->
	
	<!-- <div style='text-align: center'><input type="text" id="search" placeholder="Type to search" align="middle" style="margin: 0px auto; width: 90%; font-size: 14pt;"></div>
	<table id="friends" position='fixed' bottom=0 align="center" style="margin: 0px auto; width: 90%;  left-padding: 100px; border: 1px solid black; font-family: Arial; border-collapse: collapse; text-align: center;">
		<tr>
			<th style="text-align: center;">Friends</th>
			<th style="text-align: center;">Ranking</th>
		</tr>
		<tr>
			<td style="background-color: #BABABA;"><a href='/bitpal/friendsProfile'>YourWorstNightmare</a></td>
			<td style="background-color: #BABABA;">12</td>
		</tr>
		<tr>
			<td>FitnessFreak</td>
			<td>2</td>
		</tr>
		<tr>
			<td style="background-color: #BABABA;">GymBuff</td>
			<td style="background-color: #BABABA;">7</td>
		</tr>
		<tr>
			<td>Bob</td>
			<td>17</td>
		</tr>
		<tr>
			<td style="background-color: #BABABA;">WalkMaster</td>
			<td style="background-color: #BABABA;">53</td>
		</tr>
	</table> -->
	
