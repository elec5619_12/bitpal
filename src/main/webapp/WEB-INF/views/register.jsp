<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
</head>
<body>
<h1>
	Register Page. 
</h1>

<div class="container" style="width:40%">

      <form name="registerUser" action="register.htm" method="post" class="form-signin" role="form">
        <h2 class="form-signin-heading">Please register</h2>
        <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
        <input type="password" class="form-control" name="password" placeholder="Password" required>
        <input type="password" class="form-control" name="confirmPassword" placeholder="Confirm Password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
      </form>

</div>
${passwordRequirement}
</body>
</html>
