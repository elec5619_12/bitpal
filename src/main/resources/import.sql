INSERT INTO Item (id, name, price, type, equipped) VALUES (1, 'Master Sword', 100.0, 'WEAPON', true);
INSERT INTO Item (id, name, price, type, equipped) VALUES (2, 'Link''s Hat', 40, 'HAT', true);
INSERT INTO Item (id, name, price, type, equipped) VALUES (3, 'Link''s Tunic', 40, 'SHIRT', true);
INSERT INTO Item (id, name, price, type, equipped) VALUES (4, 'Link''s Pants', 40, 'PANTS', true);
INSERT INTO Item (id, name, price, type, equipped) VALUES (5, 'Link''s Boots', 40, 'BOOTS', true);
INSERT INTO Item (id, name, price, type, equipped) VALUES (6, 'Goron Gloves', 60, 'GLOVES', true);
INSERT INTO Item (id, name, price, type, equipped) VALUES (7, 'Goron Tunic', 60, 'SHIRT', false);

INSERT INTO Product (id, name, price, type) VALUES (8, 'Short Sword', 100.0, 'WEAPON');
INSERT INTO Product (id, name, price, type) VALUES (15, 'James Bond Pistol', 150.0, 'WEAPON');
INSERT INTO Product (id, name, price, type) VALUES (9, 'Army Cap', 15, 'HAT');
INSERT INTO Product (id, name, price, type) VALUES (14, 'Zombie-proof Helmet', 20, 'HAT');
INSERT INTO Product (id, name, price, type) VALUES (10, 'Denim Jacket', 40, 'SHIRT');
INSERT INTO Product (id, name, price, type) VALUES (11, 'Hipster Jacket', 80, 'SHIRT');
INSERT INTO Product (id, name, price, type) VALUES (12, 'Waterproof Boots', 40, 'BOOTS');
INSERT INTO Product (id, name, price, type) VALUES (16, 'Noob Sneakers', 10, 'BOOTS');
INSERT INTO Product (id, name, price, type) VALUES (17, 'Tony Abott Undies', 25, 'PANTS');
INSERT INTO Product (id, name, price, type) VALUES (18, 'Basic Denim Jeans', 20, 'PANTS');
INSERT INTO Product (id, name, price, type) VALUES (19, 'Bear Grylls Pant ', 50, 'PANTS');