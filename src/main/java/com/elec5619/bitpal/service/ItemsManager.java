package com.elec5619.bitpal.service;

import java.util.List;

import com.elec5619.bitpal.domain.Item;
import com.elec5619.bitpal.domain.Type;

public interface ItemsManager {
	public void addItem(Item item);
	public List<Item> getItems();
	public Item getItemByID(long id);
	public Type getTypeOfItem(Item item);
	public void updateItem(Item item);
	public List<Item> getEquippedItems();
	public void removeAllItems();
	
	//Christine's added method
	public List<Item> getItemsbyType(Type t);
	public Item getItemByName(String n);
	
	

}
