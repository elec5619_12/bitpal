package com.elec5619.bitpal.service;
import java.util.List;

import org.hibernate.classic.Session;
import org.hibernate.impl.QueryImpl;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.bitpal.domain.Product;
import com.elec5619.bitpal.domain.Type;
import com.elec5619.bitpal.domain.User;

@Service(value="shopManager")
@Repository
@Transactional
public class DatabaseShopManager implements ShopManager
{
	
	private SessionFactory sf;

	@Autowired
	public void setSessionFactory(SessionFactory sf)
	{
		this.sf = sf;
	}
	
	public Session getSession()
	{
		return sf.getCurrentSession();
	}
	
	@Override
	public void addItem(Product product) {
		getSession().save(product);
	}

	@Override
	public List<Product> getItems() {
		return (List<Product>) getSession().createQuery("FROM Product").list();
	}

	@Override
	public Product getItemByID(long id) {
		return (Product)getSession().get(Product.class, id);
	}
	
	public Product getItemByName(String search) {
		Query q = getSession().createQuery("FROM Product WHERE name = :n");
		q.setParameter("n", search);
		List list = q.list();
		if (list.isEmpty())
			return null;
		Product p = (Product) list.get(0);
		return p;
	}


	@Override
	public Type getTypeOfItem(Product item) {
		
		String type = (String) getSession().createQuery("SELECT Type FROM Product WHERE name="+item.getName()).iterate().next();
		return Type.valueOf(type);
	}
	
	@Override
	public void updateItem(Product item)
	{
		getSession().update(item);
	}
	
	public void removeAllItems()
	{
		getSession().createQuery("delete from Product").executeUpdate();
	}

	//CHRISTINES ADDED METHOD
	@Override
	public List<Product> getItemsbyType(Type t) {
		
		return (List<Product>) getSession().createQuery("FROM Product WHERE Type=:T").setParameter("T",t.toString()).list();
	
	}
 
}