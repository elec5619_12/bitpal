package com.elec5619.bitpal.service;

import java.util.List;

import org.hibernate.classic.Session;
import org.hibernate.impl.QueryImpl;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.bitpal.domain.Item;
import com.elec5619.bitpal.domain.Product;
import com.elec5619.bitpal.domain.Type;

@Service(value="itemManager")
@Repository
@Transactional
public class DatabaseItemsManager implements ItemsManager
{
	
	private SessionFactory sf;

	@Autowired
	public void setSessionFactory(SessionFactory sf)
	{
		this.sf = sf;
	}
	
	public Session getSession()
	{
		return sf.getCurrentSession();
	}
	
	@Override
	public void addItem(Item item) {
		getSession().save(item);
	}

	@Override
	public List<Item> getItems() {
		return (List<Item>) getSession().createQuery("FROM Item").list();
	}

	@Override
	public Item getItemByID(long id) {
		return (Item) getSession().get(Item.class, id);
	}

	@Override
	public Type getTypeOfItem(Item item) {
		
		String type = (String) getSession().createQuery("SELECT Type FROM Item WHERE name="+item.getName()).iterate().next();
		return Type.valueOf(type);
	}
	
	@Override
	public void updateItem(Item item)
	{
		getSession().update(item);
	}
	
	@Override
	public List<Item> getEquippedItems()
	{
		return (List<Item>) getSession().createQuery("FROM Item WHERE equipped=true").list();
	}
	
	public void removeAllItems()
	{
		getSession().createQuery("delete from Item").executeUpdate();
	}

	//CHRISTINES ADDED METHOD
	@Override
	public List<Item> getItemsbyType(Type t) {
		
		return (List<Item>) getSession().createQuery("FROM Item WHERE Type=:T").setParameter("T",t.toString()).list();
	
	}
	
	@Override
	public Item getItemByName(String search) {
		Query q = getSession().createQuery("FROM Item WHERE name = :n");
		q.setParameter("n", search);
		List list = q.list();
		if (list.isEmpty())
			return null;
		Item p = (Item) list.get(0);
		return p;
	}


 
}
