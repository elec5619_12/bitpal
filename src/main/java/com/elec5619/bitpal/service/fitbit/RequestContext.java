package com.elec5619.bitpal.service.fitbit;

import org.joda.time.LocalDate;

import com.fitbit.api.client.FitbitApiClientAgent;
import com.fitbit.api.client.LocalUserDetail;
import com.fitbit.api.client.service.FitbitAPIClientService;


public class RequestContext {

    private LocalDate parsedLocalDate = new LocalDate();
    private FitbitAPIClientService<FitbitApiClientAgent> apiClientService;
    private LocalUserDetail ourUser;

    public LocalDate getParsedLocalDate() {
        return parsedLocalDate;
    }

    public FitbitAPIClientService<FitbitApiClientAgent> getApiClientService() {
        return apiClientService;
    }

    public void setApiClientService(FitbitAPIClientService<FitbitApiClientAgent> apiClientService) {
        this.apiClientService = apiClientService;
    }

    public LocalUserDetail getOurUser() {
        return ourUser;
    }

    public void setOurUser(LocalUserDetail ourUser) {
        this.ourUser = ourUser;
    }
}
