package com.elec5619.bitpal.service;

import com.elec5619.bitpal.domain.User;

public class Login {

	public User user;
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public User getUser() {
		return user;
	}
	
}
