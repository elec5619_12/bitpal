package com.elec5619.bitpal.service;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class LoginValidator implements Validator {

	@Override
	public boolean supports(Class clazz) {
		return Login.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object obj, Errors err) {
		Login login = (Login) obj;
		if (login.getUser() == null) {
			err.rejectValue("User", "error.empty.field", "Please Enter a User Name");
		}
		// and so on
	}
	
	

}
