package com.elec5619.bitpal.service;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.bitpal.domain.Item;
import com.elec5619.bitpal.domain.User;
import com.fitbit.api.common.model.activities.Activities;
import com.fitbit.api.common.model.foods.Foods;
import com.fitbit.api.common.model.foods.Water;
import com.fitbit.api.common.model.sleep.Sleep;
import com.fitbit.api.common.model.user.Gender;
import com.fitbit.api.common.model.user.UserInfo;

public class DatabaseTestUserManager implements UserManager {
	
	public User user;
	public List<User> friends = new ArrayList <User>();
	
	public User login(String username, String password) throws NoSuchAlgorithmException, InvalidKeySpecException
	{
		User user = getUserByName(username);
		
		if (user == null)
		{
			return null;
		}
		
		if(!(PasswordHash.validatePassword(password, user.getPassword())))
		{
			return null;
		}
		
		
		
		return user;
		
	}

	@Override
	public void register(String username, String password) {
		User user = new User();
		
		user.setDummyAttributes();
		
		user.setUsername(username);
		user.setPassword(password);
		
		
		this.addUser(user);
	}

	@Override
	public void signoff() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void changePassword(User user) {
		 this.user.setPassword(user.getPassword());
	}

	@Override
	public User getUserById(long id) {
		return user;
		
	}

	@Override
	public void addUser(User user) {
		this.user = user;
	}
	
	@Override
	public List<User> getUsers() {
		List<User> users = new ArrayList<User>();
		users.add(user);
		return users;
	}
	
	@Override
	public User getUserByName(String username) {
		//Query q = getSession().createQuery("FROM User WHERE Username = :username");
		//q.setParameter("username", username);
		//List list = q.list();
		List list = null;
		if (list.isEmpty())
			return null;
		User user = (User) list.get(0);
		return user;
	}
	
	@Override
	public void updateUserInfo(long id, UserInfo userInfo) {
		User user = this.getUserById(id);
		user.setUserInfo(userInfo);
		
	}
	
	public void updateUserAttributes(long id, List<Activities> activities,
		List<Sleep> sleep, List<Foods> foods, List<Water> water) {
		
		User user = this.getUserById(id);
		
		int act = 0;
		int sle = 0;
		int wat = 0;
		int foo = 0;
		
		for (int i = 0; i < foods.size(); i++) {
			act += getActivityScore(activities.get(i));
			sle += getSleepScore(sleep.get(i));
			wat += getWaterScore(water.get(i));
			foo += getFoodScore(foods.get(i));
		}
		
		user.setAttack(1 + foo + (int) act/3);
		user.setDefense(1 + sle + wat + act/10);
		user.setHealth(3 + act);
		user.setExperience(user.getExperience() + 20 + (act+sle+wat+foo));
		user.setLastUpdated(new LocalDate().toString());
		
		
		
	}
	
	// activity score of one day (0-30)
	public int getActivityScore(Activities activity) {
		int score = 0;
		int steps = activity.getSummary().getSteps();
		if (steps >= 10000)
			score += 2;
		return score + (int) (steps/2000) + 1;
	}
	
	// sleep score (0-2)
	public int getSleepScore(Sleep sleep) {
		if (sleep.getSummary().getTotalMinutesAsleep() <= 0)
			return 0;
		else
			return 2;
	}
	
	//food score (0-10)
	public int getFoodScore(Foods food) {
		int score = 0;
		int calories = food.getSummary().getCalories();
		if (calories <= 0)
			score = 0;
		else if (calories > 1000 && calories <= 3000)
			score = 10;
		else
			score = 5;
		
		return score;
		
	}
	
	// water score (0-8)
	public int getWaterScore(Water water) {
		int score = 0;
		double waterConsumed = water.getSummary().getWater();
		// assumed in litres
		if (waterConsumed == 0)
			score = 0;
		else if (waterConsumed < 1.0)
			score+=2;
		else if (waterConsumed < 2.0)
			score+=2;
		else if (waterConsumed < 3.0)
			score+=2;
		else if (waterConsumed < 4.0)
			score+=2;
		else if (waterConsumed < 5.0)
			score+=2;
		else 
			score=2;
		
		return score;
	}

	@Override
	public void updateUser(User user) {
		
		
	}
	

	@Override
	public List<User> getFriends() {
		return friends;
		
		//return (List<User>) getSession().createQuery("FROM User WHERE isFriend = 1").list();
	}
	
	public void addFriend(User u) {
		friends.add(u);
	}


}
