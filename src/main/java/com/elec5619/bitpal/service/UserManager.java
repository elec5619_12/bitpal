package com.elec5619.bitpal.service;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import com.elec5619.bitpal.domain.Item;
import com.elec5619.bitpal.domain.User;
import com.fitbit.api.common.model.activities.Activities;
import com.fitbit.api.common.model.foods.Foods;
import com.fitbit.api.common.model.foods.Water;
import com.fitbit.api.common.model.sleep.Sleep;
import com.fitbit.api.common.model.user.UserInfo;

public interface UserManager {
	public void addUser(User user);
	public List<User> getUsers();
	public void register(String username, String password);
	public void signoff();
	public void changePassword(User user);
	public User getUserById(long id);
	public User getUserByName(String username);
	public User login(String username, String password) throws NoSuchAlgorithmException, InvalidKeySpecException;
	public void updateUserInfo(long id, UserInfo userInfo);
	public void updateUserAttributes(long id, List<Activities> activities,
			List<Sleep> sleep, List<Foods> foods, List<Water> water);
	
	/* Hayleys */
	public void updateUser(User user);
	public List<User> getFriends();
	
}
