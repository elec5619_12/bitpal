package com.elec5619.bitpal.service;

import java.util.List;


import com.elec5619.bitpal.domain.Product;
import com.elec5619.bitpal.domain.Type;

public interface ShopManager {
	public void addItem(Product item);
	public List<Product> getItems();
	public Product getItemByID(long id);
	public Product getItemByName(String name);
	public Type getTypeOfItem(Product item);
	public void updateItem(Product item);
	public void removeAllItems();
	
	//Christine's added method
	public List<Product> getItemsbyType(Type t);	

	

}
