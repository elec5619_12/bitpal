package com.elec5619.bitpal.domain;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="FitBitData")
public class FitBitData implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="Id")
	private long id;
	
	@Column(name="Date")
	private Date date;
	
	@Column(name="Weight")
	private int weight;
	
	@Column(name="BMI")
	private int bmi;
	
	@Column(name="Fat")
	private int fat;
	
	@Column(name="CaloriesIn")
	private int caloriesIn;
	
	@Column(name="CaloriesOut")
	private int caloriesOut;
	
	@Column(name="Steps")
	private int steps;
	
	@Column(name="Distance")
	private int distance;
	
	@Column(name="Floors")
	private int floors;
	
	//Minutes Lightly Active
	@Column(name="MLA")
	private int mla;
	
	//Minutes Fairly Active	
	@Column(name="MFA")
	private int mfa;
	
	//Minutes Very Active	
	@Column(name="MVA")
	private int mva;
	
	//Activity Calories
	@Column(name="CaloriesAct")
	private int caloriesAct;
	
	@Column(name="MinutesAsleep")
	private int minutesAsleep;
	
	@Column(name="MinutesAwake")
	private int minutesAwake;
	
	@Column(name="NumberOfAwakenings")
	private int numberOfAwakenings;
	
	@Column(name="TimeInBed")
	private int timeInBed;


}
