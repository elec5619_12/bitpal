package com.elec5619.bitpal.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fitbit.api.common.model.activities.Activities;
import com.fitbit.api.common.model.foods.Foods;
import com.fitbit.api.common.model.foods.Water;
import com.fitbit.api.common.model.sleep.Sleep;
import com.fitbit.api.common.model.user.Gender;
import com.fitbit.api.common.model.user.UserInfo;

@Entity
@Table(name="User")
public class User implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	@Id
	@GeneratedValue
	@Column(name="Id")
	private long id;
	
	@Column(name="EncodedId")
	private String encodedId;
	
	@Column(name="DisplayName")
	private String displayName;
	
	@Column(name="MemberSince")
	private String memberSince;
	
	@Column(name="Username")
	private String username;
	
	@Column(name="Password")
	private String password;
	
	@Column(name="Weight")
	private double weight;
	
	@Column(name="Height")
	private double height;

	@Column(name="Gender")
	private Gender gender;
	
	@Column(name="Timezone")
	private String timezone;
	
	@Column(name="DateOfBirth")
	private String dateOfBirth;
	
	@Column(name="Locale")
	private String locale;
	
	@Column(name="Friend")
	private boolean isFriend;
	
	/* */
	
	@Column(name="DistanceUnit")
	private String distanceUnit;
	
	@Column(name="HeightUnit")
	private String heightUnit;
	
	@Column(name="WeightUnit")
	private String weightUnit;
	
	@Column(name="WaterUnit")
	private String waterUnit;
	
	/*
	 * Monster Stats
	 */
	
	@Column(name="Experience")
	private long experience;
	
	@Column(name="Health")
	private int health;
	
	@Column(name="Defense")
	private int defense;
	
	@Column(name="Attack")
	private int attack;
	
	@Column(name="Score")
	private int score;
	
	@Column(name="LastUpdated")
	private String lastUpdated;
	
	@Column(name="Balance")
	private int balance;
	
	public User() {
		this.experience = 0;
	}
	
	
	public void setUserInfo(UserInfo userInfo) {
		this.encodedId = userInfo.getEncodedId();
		this.displayName = userInfo.getDisplayName();
		this.memberSince = userInfo.getMemberSince().toString();
		
		this.weight = userInfo.getWeight();
		this.height = userInfo.getHeight();
		this.gender = userInfo.getGender();
		this.timezone = userInfo.getTimezone();
		this.dateOfBirth = userInfo.getDateOfBirth();
		this.locale = userInfo.getLocale();
		
		this.distanceUnit = userInfo.getDistanceUnit();
		this.heightUnit = userInfo.getHeightUnit();
		this.weightUnit = userInfo.getWeightUnit();
		this.waterUnit = userInfo.getWaterUnit();
	}
	
	// possible add more stuff
	public void setDummyAttributes() {
		
		setExperience(getExperience() + 10);
		setHealth(3);
		setDefense(1);
		setAttack(2);
		setScore();
		
	}
	
	private void updateExperience() {
		
	}
	public void setHealth(int health) {
		this.health = health;
	}
	public void setDefense(int defense) {
		this.defense = defense;
	}
	public void setAttack(int attack) {
		this.attack = attack;
	}	
	public void setScore() {
		this.score = (int) (attack + defense + health + experience);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String string) {
		this.dateOfBirth = string;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getDistanceUnit() {
		return distanceUnit;
	}

	public void setDistanceUnit(String distanceUnit) {
		this.distanceUnit = distanceUnit;
	}

	public String getHeightUnit() {
		return heightUnit;
	}

	public void setHeightUnit(String heightUnit) {
		this.heightUnit = heightUnit;
	}

	public String getWeightUnit() {
		return weightUnit;
	}

	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}

	public String getWaterUnit() {
		return waterUnit;
	}

	public void setWaterUnit(String waterUnit) {
		this.waterUnit = waterUnit;
	}

	public long getExperience() {
		return experience;
	}

	public void setExperience(long experience) {
		this.experience = experience;
	}

	public int getHealth() {
		return health;
	}

	public int getDefense() {
		return defense;
	}
	


	public int getAttack() {
		return attack;
	}

	public String getEncodedId() {
		return encodedId;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public String getMemberSince() {
		return memberSince;
	}
	
	
	public void setBalance(int no) {
		this.balance = no;
	}
	
	public void getBalance(int no) {
		this.balance = no;
	}


	public String getLastUpdated() {
		return lastUpdated;
	}


	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}


	public void setEncodedId(String encodedId) {
		this.encodedId = encodedId;
	}


	public void setMemberSince(String memberSince) {
		this.memberSince = memberSince;
	}
	
	public boolean isFriend() {
		return isFriend;
	}
	
	public void setFriend(boolean isFriend) {
		this.isFriend = isFriend;
	}

}
