package com.elec5619.bitpal.domain;

public enum Type
{
	SHIRT, PANTS, BOOTS, GLOVES, HAT, WEAPON
}
