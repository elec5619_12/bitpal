package com.elec5619.bitpal.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name="Product")
public class Product
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="Id")
	private long Id;

	@Column(name="name")
	private String name;
	
	@Column(name="price")
	private Double price;
	
	@Enumerated(EnumType.STRING)
	@Column(name="Type")
	private Type type;
	
	////////////////////////////////////////
	
	public long getId() {
		return Id;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setPrice(double price)
	{
		this.price = price;
	}
	
	public Double getPrice()
	{
		return price;
	}
	
	public void setType(Type type)
	{
		this.type = type;
	}
	
	public Type getType()
	{
		return type;
	}
}
