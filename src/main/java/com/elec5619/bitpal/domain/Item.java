package com.elec5619.bitpal.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Item")
public class Item 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="Id")
	private long id;

	@Column(name="Name")
	private String name;
	
	@Column(name="Price")
	private Double price;
	
	@Enumerated(EnumType.STRING)
	@Column(name="Type")
	private Type type;
	
	@Column(name="Equipped")
	private boolean isEquipped;
	

	
	////////////////////////////////////////
	
	public long getId() {
		return id;
	}
	
	public void setId(long ID) {
		this.id=ID;
	}


	public boolean isEquipped() {
		return isEquipped;
	}

	public void setEquipped(boolean isEquipped) {
		this.isEquipped = isEquipped;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setPrice(double price)
	{
		this.price = price;
	}
	
	public Double getPrice()
	{
		return price;
	}
	
	public void setType(Type type)
	{
		this.type = type;
	}
	
	public Type getType()
	{
		return type;
	}
}
