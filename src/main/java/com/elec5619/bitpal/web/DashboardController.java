package com.elec5619.bitpal.web;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.elec5619.bitpal.domain.User;
import com.elec5619.bitpal.service.UserManager;

@Controller
public class DashboardController 
{
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Resource(name="userManager")
	private UserManager userManger;
	/**
	 * Show dashboard page
	 */
	@RequestMapping(value ="/dashboard", method = RequestMethod.GET)
	public String dashboard(Locale locale, Model model, HttpServletRequest httpServletRequest)
	{
		return "redirect:/";
	}
	
	
}
