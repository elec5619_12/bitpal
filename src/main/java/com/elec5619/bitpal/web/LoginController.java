package com.elec5619.bitpal.web;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.elec5619.bitpal.domain.User;
import com.elec5619.bitpal.service.UserManager;

@Controller
public class LoginController {
	
private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Resource(name="userManager")
	UserManager userManager;
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(HttpServletRequest httpServletRequest, HttpServletResponse response, Model model) {
		
		
		// If already logged in. Log out!
		//if (session.getAttribute("authenticated") != null) {
		//	return "redirect:dashboard";
		//}
		
		HttpSession session = httpServletRequest.getSession();
		
		
		
		if(!(session.getAttribute("Authentication") == null))
		{
			if(session.getAttribute("Authentication").equals("false"))
			{
				if(!(session.getAttribute("AttemptedLogin") == null))
				{
					if(session.getAttribute("AttemptedLogin").equals("true"))
					{
						model.addAttribute("authenticationMessage", "Authentication failed");
					
					}
				
						model.addAttribute("authenticationMessage", "");
				}
				
				model.addAttribute("authenticationMessage", "");
			
			}
			
			else if (session.getAttribute("Authentication").equals("true"))
			{
//				model.addAttribute("authenticationMessage", "You are already logged in!");
				return "home";
			}
			
			else
				model.addAttribute("authenticationMessage", "");
		}
		
				
		
		else
		{
			model.addAttribute("authenticationMessage", "");
			
		}
		
		return "login";
	}
	
	/**
	 * Very basic verification of login username and password
	 * @throws InvalidKeySpecException 
	 * @throws NoSuchAlgorithmException 
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String verifyLogin(HttpServletRequest httpServletRequest, Model model) throws NoSuchAlgorithmException, InvalidKeySpecException
	{
		String username = httpServletRequest.getParameter("username");
		String password = httpServletRequest.getParameter("password");
		User user = userManager.login(username, password);
		HttpSession session = httpServletRequest.getSession();
		
		if (user  != null)
		{	
			session.setAttribute("AuthenticatedUser", user);
			session.setAttribute("Authentication", "true");
			return "redirect:/authorize.html";

		}
		
		else
		{
			session.removeAttribute("AttemptedLogin");
			session.setAttribute("AttemptedLogin", "true");
			session.setAttribute("Authentication", "false");
			model.addAttribute("authenticationMessage", "Authentication failed");
			return "login";
		}
		
		
		//lastUpdated
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public String logout(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		session.invalidate();
		return "redirect:/login";
	}
	

}
