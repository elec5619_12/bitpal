package com.elec5619.bitpal.web;

import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.elec5619.bitpal.domain.User;
import com.elec5619.bitpal.service.UserManager;

@Controller
public class BattleController {
	User friendUser;
	long friendScore;
	long userScore;
	@Resource(name="userManager")
	UserManager userManager;
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/battle", method = RequestMethod.GET)
	public String login(Locale locale, Model model, HttpServletRequest httpServletRequest) {
		HttpSession session = httpServletRequest.getSession();
		model.addAttribute("self", session.getAttribute("AuthenticatedUser"));
		model.addAttribute("friend", friendUser);
		model.addAttribute("userScore", userScore);
		model.addAttribute("friendScore", friendScore);

		return "battle";
	}
	
	@RequestMapping(value = "/battle", method = RequestMethod.POST)
		public String battle(HttpServletRequest httpServletRequest) {
		String self = httpServletRequest.getParameter("self");
		String friend = httpServletRequest.getParameter("friend");
		System.out.println("self = " + self);
		System.out.println("friend = " + friend);
		friendUser = userManager.getUserByName(friend);
		User selfUser = userManager.getUserByName(self);
		friendScore = friendUser.getAttack() + friendUser.getDefense() + friendUser.getExperience() + friendUser.getHealth();
		userScore = selfUser.getAttack() + selfUser.getDefense() + selfUser.getExperience() + selfUser.getHealth();
		System.out.println("Friendscore = " + friendScore);
		System.out.println("Userscore = " + userScore);
			
		return "redirect:/battle";
		
	}

}
