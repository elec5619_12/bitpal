package com.elec5619.bitpal.web;

import java.util.List;

import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.elec5619.bitpal.domain.Item;
import com.elec5619.bitpal.domain.User;
import com.elec5619.bitpal.service.ItemsManager;
import com.elec5619.bitpal.service.UserManager;

@Controller
public class FriendsController {
	
	//User self;

	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Resource(name="userManager")
	UserManager userManager;
	@RequestMapping(value = "/friends", method = RequestMethod.GET)
	public String login(Locale locale, Model model, HttpServletRequest httpServletRequest) {

		HttpSession session = httpServletRequest.getSession();
		
		
		if(session.getAttribute("AuthenticatedUser") == null)
		{
			return "login";
		}
		
		model.addAttribute("users", userManager.getUsers());
		model.addAttribute("friends", userManager.getFriends());
		
		model.addAttribute("self", session.getAttribute("AuthenticatedUser"));
	
		
		//List <User> user = userManager.getUsers();
		
		return "friends";
	}

	@RequestMapping(value = "/friends", method = RequestMethod.POST)
	public String getFriends(HttpServletRequest httpServletRequest) {
		
		String idString = httpServletRequest.getParameter("id");
		String current = httpServletRequest.getParameter("current");
		//HttpSession session = httpServletRequest.getSession();
		
		long id = Long.parseLong(idString);
		User user = userManager.getUserById(id);
		//self = (User) session.getAttribute("authenticated");
		
		if (current.equals("Add Friend")) {
			user.setFriend(true);
			userManager.updateUser(user);
			
		} else {
			user.setFriend(false);
			userManager.updateUser(user);
		}
		
		//List <User> users = userManager.getUsers();
		//List <User> friends = userManager.getFriends();
		
		//return users; // return page you want to get to
		return "redirect:/friends";
		
	}


}
