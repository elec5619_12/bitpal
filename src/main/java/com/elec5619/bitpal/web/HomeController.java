package com.elec5619.bitpal.web;

import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.elec5619.bitpal.service.UserManager;


/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	@Resource(name="userManager")
	UserManager userManager;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model, HttpServletRequest httpServletRequest) {
		HttpSession session = httpServletRequest.getSession();
		if (session.getAttribute("Authentication") == null)
			return "redirect:login";
		if (session.getAttribute("Authentication").equals("false"))
			return "redirect:login";
		
		model.addAttribute("user", session.getAttribute("AuthenticatedUser"));
		
//		session.setAttribute("Authentication", "false");
//		session.setAttribute("AttemptedLogin", "false");
		
		return "home";
	}
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home2(Locale locale, Model model, HttpServletRequest httpServletRequest) {
	
		return "redirect:/";
	}
	
	
	
	
	
	
	
}
