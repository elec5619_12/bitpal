package com.elec5619.bitpal.web;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.taglibs.standard.tag.common.core.RemoveTag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.elec5619.bitpal.domain.Item;
import com.elec5619.bitpal.domain.Product;
import com.elec5619.bitpal.domain.Type;
import com.elec5619.bitpal.domain.User;
import com.elec5619.bitpal.service.ItemsManager;
import com.elec5619.bitpal.service.ShopManager;
import com.elec5619.bitpal.service.UserManager;

@Controller
public class ShopController {
	
	@Resource(name="itemManager")
	private ItemsManager itemManager;
	
	@Resource(name="shopManager")
	private ShopManager shopManager;
	
	@Resource(name="userManager")
	private UserManager userManager;
	

	@Autowired(required = false)
	private Model model;
	
	private int flag=4; //0-purchased 1-not enough coins 2- already owned 3-null -4 testing
	private User usr;
	
    private static final Logger logger = LoggerFactory.getLogger(ShopController.class);

    /**
     * Displays Store contents organised by Category
     *  **/
    @RequestMapping(value = "/shop", method = RequestMethod.GET)
    public String shopview(Model model, HttpServletRequest httpServletRequest) {

		logger.info("Welcome to the Store");
		HttpSession session = httpServletRequest.getSession();
		User usr = (User) session.getAttribute("AuthenticatedUser");
		
		if (usr!=null) {
			if (flag==4) {
				usr.setExperience(100);
				logger.info("Active user logged in is "+usr.getUsername());
			}
		}
	
		
		
		
		List<Product> TOPS = shopManager.getItemsbyType(Type.SHIRT); 
		List<Product> PANTS = shopManager.getItemsbyType(Type.PANTS);
		List<Product> WEAPONS = shopManager.getItemsbyType(Type.WEAPON);
		List<Product> BOOTS = shopManager.getItemsbyType(Type.BOOTS);
		List<Product> HATS = shopManager.getItemsbyType(Type.HAT);
		
		model.addAttribute("tops",TOPS);
		model.addAttribute("pants",PANTS);
		model.addAttribute("weapons",WEAPONS);
		model.addAttribute("boots",BOOTS);
		model.addAttribute("hat",HATS);	
		if (usr==null) 
			model.addAttribute("balance","0");
		else 
			model.addAttribute("balance",usr.getExperience());
		
        return "shop";      
    }
    
 
    /** **/
    @RequestMapping(value = "/shop", method = RequestMethod.POST)
    public String viewItem(@RequestParam("name")long name, Model model,HttpServletRequest httpServletRequest, HttpServletResponse response) throws Exception {
    	
    	HttpSession session = httpServletRequest.getSession();
		User usr = (User) session.getAttribute("AuthenticatedUser");
		
    	if (usr==null) {
    		logger.info("User needs to login");
    		return "redirect:/login";
    	}
    	
    	logger.info(usr.getUsername()+" wants to purchase "+String.valueOf(name));
    	Product p = shopManager.getItemByID(name);
    	logger.info("Retrieved "+p.getName());
    	

    	//Converting product into an item
    	Item item = new Item();
    	item.setId(p.getId());
    	item.setName(p.getName());
    	item.setPrice(p.getPrice());
    	item.setType(p.getType());
    	item.setEquipped(false);
    	
    	
    	
    	Item check = itemManager.getItemByName(item.getName());
    	if (check==null) {
    		logger.info("NO DUPLICATE");
    		if (usr.getExperience()>=item.getPrice()) {
    			itemManager.addItem(item);
    			usr.setExperience((usr.getExperience()-item.getPrice().longValue()));
    			flag=1;
    			return "redirect:/confirmed.htm";
    		} 
    		else {
    			flag=0;
    			return"redirect:/confirmed.htm";
    		}
    	}
    	else {
    		logger.info("Already have this in the inventory. Redirecting...");
    		flag=3;
    		model.addAttribute("msg","You already have that in your inventory");
    		return "redirect:/confirmed";  	
    	}
    } 
    
    
	@RequestMapping(value = "/confirmed", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Confirmation page");
		
		if (flag==0)
			model.addAttribute("msg","You do not have enough experience.. go get some exercise ");
		else if (flag==1)
			model.addAttribute("msg","Purchase completed. The item should now be in your Inventory!");
		else //flag=3
			model.addAttribute("msg","You already have this item in your Inventory..");
		
		
		return "/confirmed";
	}
    

    

   
}

	
