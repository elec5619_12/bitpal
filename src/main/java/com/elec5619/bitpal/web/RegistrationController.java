package com.elec5619.bitpal.web;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.elec5619.bitpal.domain.User;
import com.elec5619.bitpal.service.PasswordHash;
import com.elec5619.bitpal.service.UserManager;

@Controller
public class RegistrationController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Resource(name="userManager")
	UserManager userManager;
	/**
	 * Show Register page
	 */
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String register(Locale locale, Model model, HttpServletRequest httpServletRequest) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		HttpSession session = httpServletRequest.getSession();
		
		if (session.getAttribute("RegisterError") != null)
		{
			if (session.getAttribute("RegisterError").equals("PasswordLength"))
			{
				model.addAttribute("passwordRequirement", "Please choose a password between 6 to 15 characters in length");
			}
			
			else if(session.getAttribute("RegisterError").equals("UnconfirmedPassword"))
			{
				model.addAttribute("passwordRequirement", "Please make sure you confirm your password is correct!");
			}
			
			else if (session.getAttribute("RegisterError").equals("UsernameUsed"))
			{
				model.addAttribute("passwordRequirement", "That Username is already registered");
			}
		}
		
		return "register";
	}
	
	/**
	 * User has sent information for registration
	 * @throws InvalidKeySpecException 
	 * @throws NoSuchAlgorithmException 
	 */
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String submitRegistration(HttpServletRequest httpServletRequest, Model model) throws NoSuchAlgorithmException, InvalidKeySpecException {
		
		String password = httpServletRequest.getParameter("password");
		String confirmPassword = httpServletRequest.getParameter("confirmPassword");
		
		HttpSession session = httpServletRequest.getSession();
		
		String username = httpServletRequest.getParameter("username");
		
		if (userManager.getUserByName(username) != null)
		{
			session.setAttribute("RegisterError", "UsernameUsed");
			return "redirect:/register.htm";
		}
		
		if(password.equals(confirmPassword))
		{
			if (password.length() < 6 || password.length() > 15)
			{
				session.setAttribute("RegisterError", "PasswordLength");
				return "redirect:/register.htm";
			}
			
			else
			{
				User user = new User();
				user.setPassword(PasswordHash.createHash(httpServletRequest.getParameter("password")));
				user.setUsername(httpServletRequest.getParameter("username"));
				userManager.addUser(user);

				return "redirect:/login.htm";
			}
				
		

		}
		
		else
		{
			session.setAttribute("RegisterError", "UnconfirmedPassword");
			
			return "redirect:/register.htm";
		}
		
	
		
	}
}
