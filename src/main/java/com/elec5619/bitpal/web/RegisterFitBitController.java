package com.elec5619.bitpal.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.elec5619.bitpal.domain.User;
import com.elec5619.bitpal.service.UserManager;
import com.elec5619.bitpal.service.fitbit.RequestContext;
import com.fitbit.api.FitbitAPIException;
import com.fitbit.api.client.FitbitAPIEntityCache;
import com.fitbit.api.client.FitbitApiClientAgent;
import com.fitbit.api.client.FitbitApiCredentialsCache;
import com.fitbit.api.client.FitbitApiCredentialsCacheMapImpl;
import com.fitbit.api.client.FitbitApiEntityCacheMapImpl;
import com.fitbit.api.client.FitbitApiSubscriptionStorage;
import com.fitbit.api.client.FitbitApiSubscriptionStorageInMemoryImpl;
import com.fitbit.api.client.LocalUserDetail;
import com.fitbit.api.client.service.FitbitAPIClientService;
import com.fitbit.api.common.model.activities.Activities;
import com.fitbit.api.common.model.activities.ActivityLog;
import com.fitbit.api.common.model.foods.Foods;
import com.fitbit.api.common.model.foods.Water;
import com.fitbit.api.common.model.sleep.Sleep;
import com.fitbit.api.common.model.units.UnitSystem;
import com.fitbit.api.common.model.user.UserInfo;
import com.fitbit.api.model.APICollectionType;
import com.fitbit.api.model.APIResourceCredentials;
import com.fitbit.api.model.ApiRateLimitStatus;
import com.fitbit.api.model.ApiSubscription;
import com.fitbit.api.model.FitbitUser;

@Controller
public class RegisterFitBitController implements InitializingBean {
	
	private static final Logger log = LoggerFactory.getLogger(RegisterFitBitController.class);
	
	@Resource(name="userManager")
	private UserManager userManager;
	
	//@Resource
	//private DatabaseUserManager reg = new DatabaseUserManager();
	
	public static final String OAUTH_TOKEN = "oauth_token";
    public static final String OAUTH_VERIFIER = "oauth_verifier";
	
	
	private FitbitAPIEntityCache entityCache = new FitbitApiEntityCacheMapImpl();
    private FitbitApiCredentialsCache credentialsCache = new FitbitApiCredentialsCacheMapImpl();
    private FitbitApiSubscriptionStorage subscriptionStore = new FitbitApiSubscriptionStorageInMemoryImpl();

    private String apiBaseUrl = "api.fitbit.com";
    private String fitbitSiteBaseUrl = "http://www.fitbit.com";
    private String exampleBaseUrl = "http://localhost:8080/bitpal";
    private String clientConsumerKey = "b81533c7e20e4157a5d6c46aa4467cd6";
    private String clientSecret = "33113ca793174263a953e418a42d0728";
    
    private FitbitAPIClientService<FitbitApiClientAgent> apiClientService;
    private LocalUserDetail lud = new LocalUserDetail("26YQSV");
    
    
    private String username;
	private String password;
	
	/*
	 * Set the properties of the apiClientService so we can utilise
	 * it across the session while it's active
	 */
	@Override
    public void afterPropertiesSet() throws Exception {
        apiClientService = new FitbitAPIClientService<FitbitApiClientAgent>(
                new FitbitApiClientAgent(apiBaseUrl, fitbitSiteBaseUrl, credentialsCache),
                clientConsumerKey,
                clientSecret,
                credentialsCache,
                entityCache,
                subscriptionStore
        );
    }
    
//	/**
//	 * Show Register page
//	 */
//	@RequestMapping(value = "/register", method = RequestMethod.GET)
//	public String register(HttpServletRequest request, HttpServletResponse response) {
//		RequestContext rq = new RequestContext();
//		return "register";
//	}
//	
//	@RequestMapping(value = "/register", method = RequestMethod.POST)
//	public String submitRegistration(HttpServletRequest request, HttpServletResponse response) {
//		List<String> errors;
//		
//		// Register the user
//		// Get username as password
//        username = (String) request.getParameter("username");
//        password = (String) request.getParameter("password");
//        
//        // Ensure username doesn't already exist
//        // Ensure password is not less than 5 characters long
//        errors = validateRegistration(username, password);
//       
//        // If any errors, return to register page with errors
//        if (errors.size() > 0) {
//        	request.setAttribute("errors", errors);
//        	return register(request, response);
//        }
//        
//        userManager.register(username, password);
//        return "login";
//	}
	
	/**
	 * The showAuthorize method contacts the FitBit API and authorises whether or not
	 * BitPal can retrieve the users data. Here we get the users username and password
	 * and check if the username exists. If so, the user is redirected and given
	 * the message. If the password is not appropriate, the user will be notified.
	 * If all is successful, the user will have successfully registered and will
	 * be redirected to the login page
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/authorize")
    public String showAuthorize(HttpServletRequest request, HttpServletResponse response) {
		
		/*
		 * CHECK IF USER IS LOGGED IN
		 */
		
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("AuthenticatedUser");

		if (u == null) {
			return "redirect:home";
		}

		RequestContext context = new RequestContext();
        populate(context, request, response);
        
        String today = new LocalDate().toString();
        if (today.equals(u.getLastUpdated())) {
        	return "redirect:home";
        }
       
        
        try {
            // Redirect to page where user can authorize the application:
        	
            return "redirect:" + context.getApiClientService().getResourceOwnerAuthorizationURL(context.getOurUser(), exampleBaseUrl + "/completeAuthorization");
        } 
        // If there's any errors when registering, redirect the user to the same page but show the errors
        catch (FitbitAPIException e) {
        	System.out.println(e.toString());
            log.error(e.toString());
            request.setAttribute("errors", Collections.singletonList(e.getMessage()));
            return "home";
        }
    }
	
	/*
	 * Validate the registration form
	 */
	private List<String> validateRegistration(String username, String password) {
		List<String> errors = new ArrayList<String>();
		
		if (username == null || password == null) {
			return errors;
		}
		
		if (userManager.getUserByName(username) != null) {
    		errors.add("Username already exists");
    	}
        
        if (password.length() <= 5) {
        	errors.add("Password needs to be more than 5 characters long");
        }
        
        return errors;
	}

	/**
	 * The showCompleteAuthorization class is what the FitBit API returns to us
	 * @param request
	 * @param response
	 */
    @RequestMapping("/completeAuthorization")
    public void showCompleteAuthorization(HttpServletRequest request, HttpServletResponse response) {
        RequestContext context = new RequestContext();
        populate(context, request, response);

        // Get the OAuth Tokens so we can perform 
        String tempTokenReceived = request.getParameter(OAUTH_TOKEN);
        String tempTokenVerifier = request.getParameter(OAUTH_VERIFIER);
        
        // Add these to the session for further processing
        HttpSession session = request.getSession();
        session.setAttribute("oauth_token", tempTokenReceived);
        session.setAttribute("oauth_verifier", tempTokenVerifier);

        // Using the OAuth tokens, we create our APIResourceCredentials in preparation for asking
        // fitbit if we can use the clients data
        APIResourceCredentials resourceCredentials = context.getApiClientService().getResourceCredentialsByTempToken(tempTokenReceived);
        
        // If we can't do that, send an error.
        if (resourceCredentials == null) {
        	System.out.println("cred is null");
            log.error("Unrecognized temporary token when attempting to complete authorization: " + tempTokenReceived);
            request.setAttribute("errors", "Unrecognized temporary token when attempting to complete authorization: " + tempTokenReceived);
        } else {
        	System.out.println("AccessToken: " + resourceCredentials);
            // Get token credentials only if necessary:
            if (!resourceCredentials.isAuthorized()) {
                // The verifier is required in the request to get token credentials:
                resourceCredentials.setTempTokenVerifier(tempTokenVerifier);
  
                try {
                    // Get token credentials for user:
                    context.getApiClientService().getTokenCredentials(new LocalUserDetail(resourceCredentials.getLocalUserId()));
                    
                    // Create a LocalUserDetail
                    LocalUserDetail localUser = new LocalUserDetail(resourceCredentials.getLocalUserId());
                    
                    // Get the User data from fitbit
                    UserInfo userInfo = apiClientService.getClient().getUserInfo(localUser);
                    
                    session.setAttribute("context", context);
                    session.setAttribute("localUser", localUser);
                    session.setAttribute("userInfo", userInfo);
                    
                    User u = (User) session.getAttribute("AuthenticatedUser");

                    FitbitUser fitbitUser = new FitbitUser(context.getOurUser().getUserId());
                    List<Activities> activities = new ArrayList<Activities>();
                    List<Sleep> sleep = new ArrayList<Sleep>();
                    List<Foods> foods = new ArrayList<Foods>();
                    List<Water> water = new ArrayList<Water>();
            		LocalDate date;
            		
            		try {
            			for (int i = 0; i < 4; i++) {
	            			date = new LocalDate().minusDays(i);
	            			activities.add(context.getApiClientService().getActivities(localUser, date));
	            			sleep.add(context.getApiClientService().getClient().getSleep(localUser, fitbitUser, date));
	            			foods.add(context.getApiClientService().getClient().getFoods(localUser, fitbitUser, date));
	            			//foods.add(context.getApiClientService().getFoods(localUser, date));
	            			water.add(context.getApiClientService().getClient().getLoggedWater(localUser, fitbitUser, date));
            			}
            		}catch(Exception e) {
            			// fitbit session timed out, use the data we have.
            		}
            		
                    
            		u.setLastUpdated(new LocalDate().toString());
                    userManager.updateUser(u);
            		
                    /* update user core stats */
                    userManager.updateUserInfo(u.getId(), userInfo);
                    
                    /* update user attributes */
                    userManager.updateUserAttributes(u.getId(), activities, sleep, foods, water);
                    
                    session.setAttribute("AuthenticatedUser", userManager.getUserById(u.getId()));
                    
                    
                    request.getRequestDispatcher("dashboard").forward(request, response);
                    // added up ---
                } catch (FitbitAPIException e) {
                    log.error("Unable to finish authorization with Fitbit.", e);
                    request.setAttribute("errors", Collections.singletonList(e.getMessage()));
                } catch (ServletException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
            }
        }

    }
	
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @ResponseBody
	public List<User> allusers(HttpServletRequest request, HttpServletResponse response) {
    	List<User> users;
    	
    	users = userManager.getUsers();
    	
    	return users;
    }
    
	
	/**
	 * Populate checks the session for a user and queries FitBit based 
	 * on that user
	 * @param context
	 * @param request
	 * @param response
	 */
	public void populate(RequestContext context, HttpServletRequest request, HttpServletResponse response) {
        context.setApiClientService(apiClientService);

        // If no user is logged on, ask to log on
        context.setOurUser(lud);
        LocalUserDetail a = context.getOurUser();
        
        APIResourceCredentials resourceCredentials = context.getApiClientService().getResourceCredentialsByUser(a);
        boolean isAuthorized = resourceCredentials != null && resourceCredentials.isAuthorized();
        boolean isSubscribed = false;
        if (isAuthorized) {
            List<ApiSubscription> subscriptions = Collections.emptyList();
            try {
                subscriptions = apiClientService.getClient().getSubscriptions(context.getOurUser());
            } catch (FitbitAPIException e) {
                log.error("Subscription error: " + e, e);
            }
            if (null != context.getOurUser() && subscriptions.size() > 0) {
                isSubscribed = true;
            }
        }
        
        request.setAttribute("actionBean", context);
        request.setAttribute("isSubscribed", isSubscribed);
        request.setAttribute("exampleBaseUrl", exampleBaseUrl);
    }
	
	/*
	 * UserInfo userInfo = context.getApiClientService().getClient().getUserInfo(context.getOurUser());
       activities = context.getApiClientService().getActivities(getLocalUser(request, response), new LocalDate(2013, 8, 22));
	 */

	protected LocalUserDetail getLocalUser(HttpServletRequest request, HttpServletResponse response) {
        return lud;
    }
	
	protected void showHome(RequestContext context, HttpServletRequest request, HttpServletResponse response) {
        List<String> errors = new ArrayList<String>();
        if (isAuthorized(context, request)) {
            // User has token credentials. Use them to get and display user's activities and foods:
            try {
                Activities activities = context.getApiClientService().getActivities(context.getOurUser(), context.getParsedLocalDate());
                request.setAttribute("activities", activities);
            } catch (FitbitAPIException e) {
                errors.add(e.getMessage());
                log.error(e.toString());
            }
            try {
                Foods foods = context.getApiClientService().getFoods(context.getOurUser(), context.getParsedLocalDate());
                request.setAttribute("foods", foods);
            } catch (FitbitAPIException e) {
                errors.add(e.getMessage());
                log.error(e.toString());
            }
            try {
                ApiRateLimitStatus clientAndViewerRateLimitStatus = context.getApiClientService().getClientAndViewerRateLimitStatus(context.getOurUser());
                request.setAttribute("clientAndViewerRateLimitStatus", clientAndViewerRateLimitStatus);
            } catch (FitbitAPIException e) {
                errors.add(e.getMessage());
                log.error(e.toString());
            }
        }

        try {
            ApiRateLimitStatus clientRateLimitStatus = context.getApiClientService().getClientRateLimitStatus();
            request.setAttribute("clientRateLimitStatus", clientRateLimitStatus);
        } catch (FitbitAPIException e) {
            errors.add(e.getMessage());
            log.error(e.toString());
        }

        if (errors.size() > 0) {
            request.setAttribute("errors", errors);
        }


        request.setAttribute("unitSystem", UnitSystem.getUnitSystem(Locale.US));
    }

	/**
	 * Check if the user is currently authorised
	 * If so, proceed as normal, if not. Do not allow
	 * further progression
	 * @param context
	 * @param request
	 * @return
	 */
    protected boolean isAuthorized(RequestContext context, HttpServletRequest request) {
        // Get cached resource credentials:
        APIResourceCredentials resourceCredentials = context.getApiClientService().getResourceCredentialsByUser(context.getOurUser());
        boolean isAuthorized = resourceCredentials != null && resourceCredentials.isAuthorized();
        request.setAttribute("isAuthorized", isAuthorized);
        if (resourceCredentials != null) {
            request.setAttribute("encodedUserId", resourceCredentials.getResourceId());
            request.setAttribute("userProfileURL", fitbitSiteBaseUrl + "/user/" + resourceCredentials.getResourceId());
        }
        return isAuthorized;
    }

	
}
