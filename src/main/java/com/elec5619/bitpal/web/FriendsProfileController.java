package com.elec5619.bitpal.web;

import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.elec5619.bitpal.domain.User;
import com.elec5619.bitpal.service.UserManager;

@Controller
public class FriendsProfileController {
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@Resource(name="userManager")
	UserManager userManager;
	User user;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/friendsProfile", method = RequestMethod.GET)
	public String login(Locale locale, Model model, HttpServletRequest httpServletRequest) {
		HttpSession session = httpServletRequest.getSession();
		model.addAttribute("self", session.getAttribute("AuthenticatedUser"));
		model.addAttribute("friend", user);
		
		return "friendsProfile";
	}
	
	@RequestMapping(value = "/friendsProfile", method = RequestMethod.POST)
	public String getFriend(HttpServletRequest httpServletRequest) {
		String idString = httpServletRequest.getParameter("id");
		long id = Long.parseLong(idString);
		user = userManager.getUserById(id);
		return "redirect:/friendsProfile";
		
	}

}
