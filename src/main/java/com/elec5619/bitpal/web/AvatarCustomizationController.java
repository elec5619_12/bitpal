package com.elec5619.bitpal.web;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.taglibs.standard.tag.common.core.RemoveTag;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.elec5619.bitpal.domain.Item;
import com.elec5619.bitpal.domain.Type;
import com.elec5619.bitpal.service.ItemsManager;

@Controller
public class AvatarCustomizationController
{
	@Resource(name="itemManager")
	private ItemsManager itemManager;
	
	protected final Log logger = LogFactory.getLog(getClass());

	@RequestMapping(value = "/avatarcustomization", method = RequestMethod.GET)
	public String avatarcustomization(Locale locale, Model model, HttpServletRequest httpServletRequest)
	{
		HttpSession session = httpServletRequest.getSession();
		
		
		if(session.getAttribute("AuthenticatedUser") == null)
		{
			return "login";
		}
		
		
		String now = (new Date()).toString();
		logger.info("Returning view avatar customization at time " + now);
		
//		if (itemManager.getItems().isEmpty())
//		{
//			Item item = new Item();
//			item.setName("Master Sword");
//			item.setPrice(100.0);
//			item.setType(Type.WEAPON);
//			item.setEquipped(true);
//			itemManager.addItem(item);
//			
//			Item item2 = new Item();
//			item2.setName("Link's Hat");
//			item2.setPrice(40.0);
//			item2.setType(Type.HAT);
//			item2.setEquipped(true);
//			itemManager.addItem(item2);
//			
//			Item item3 = new Item();
//			item3.setName("Link's Tunic");
//			item3.setPrice(40.0);
//			item3.setType(Type.SHIRT);
//			item3.setEquipped(true);
//			itemManager.addItem(item3);
//			
//			Item item4 = new Item();
//			item4.setName("Link's Pants");
//			item4.setPrice(40.0);
//			item4.setType(Type.PANTS);
//			item4.setEquipped(true);
//			itemManager.addItem(item4);
//			
//			Item item5 = new Item();
//			item5.setName("Link's Boots");
//			item5.setPrice(40.0);
//			item5.setType(Type.BOOTS);
//			item5.setEquipped(true);
//			itemManager.addItem(item5);
//			
//			item = new Item();
//			item.setName("Goron Gloves");
//			item.setPrice(60.0);
//			item.setType(Type.GLOVES);
//			item.setEquipped(false);
//			itemManager.addItem(item);
//			
//			item = new Item();
//			item.setName("Goron's Tunic");
//			item.setPrice(60.0);
//			item.setType(Type.SHIRT);
//			item.setEquipped(false);
//			itemManager.addItem(item);
//			
//		}
				
		
		//Type type = itemManager.getTypeOfItem(item);
		
		model.addAttribute("now", now);
		//model.addAttribute("type", type);
		model.addAttribute("items",itemManager.getItems());
	
		
		return "avatarcustomization";
	}
	
	/**
	 * User has sent information for registration
	 */
	@RequestMapping(value = "/avatarcustomization", method = RequestMethod.POST)
	public String toggelEquip(HttpServletRequest httpServletRequest)
	{
		String idString = httpServletRequest.getParameter("id");
		String current = httpServletRequest.getParameter("current");
		
		long id = Long.parseLong(idString);
		
		Item item = itemManager.getItemByID(id);
		
		List<Item> equippedItems = itemManager.getEquippedItems();
		
		if (current.equals("Unequip"))
		{
			item.setEquipped(false);
			itemManager.updateItem(item);
		} else {
			for (Item i: equippedItems)
			{
				if (i.getType() == item.getType())
				{
					i.setEquipped(false);
					itemManager.updateItem(i);
				}
			}
			item.setEquipped(true);
			itemManager.updateItem(item);
		}
		
		
		return "redirect:/avatarcustomization";
	}

}
